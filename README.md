# Packer / Ansible Container

[![Latest Tag](https://img.shields.io/gitlab/v/tag/ralgar/packer-ansible?style=for-the-badge&logo=semver&logoColor=white)](https://gitlab.com/ralgar/packer-ansible/tags)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/ralgar/packer-ansible?branch=v1.0.0&label=Pipeline&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/packer-ansible/-/pipelines)
[![Software License](https://img.shields.io/badge/License-GPL--3.0-orange?style=for-the-badge&logo=gnu&logoColor=white)](https://www.gnu.org/licenses/gpl-3.0.html)
[![GitLab Stars](https://img.shields.io/gitlab/stars/ralgar/packer-ansible?color=gold&label=Stars&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/packer-ansible)

## Overview

This project is a CI pipeline component for building Packer images that
 depend on the Ansible provisioner.

## Usage

1. Add as a job to your CI pipeline.

   ```yaml
   # As a GitLab CI job
   build-templates:
     stage: build
     image: registry.gitlab.com/ralgar/packer-ansible:latest
     script:
       - packer build /packer/template/dir
   ```

1. Run the pipeline.

## License

Copyright: (c) 2022, Ryan Algar
 ([ralgar/packer-ansible](https://gitlab.com/ralgar/packer-ansible))

GNU General Public License v3.0
 (see LICENSE or [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt))
